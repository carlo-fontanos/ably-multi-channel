<?php 

require_once('vendor/autoload.php');

$messages = array();

if( $_POST ){
	$ably = new \Ably\AblyRest('Qron-Q.COZFYg:KuZiW6btWOSc0xAd');
	$channels = array('channel_1', 'channel_2', 'channel_3');
	
	foreach( $channels as $channel ){
		$instance = $ably->channels->get($channel);
		$result = $instance->publish('CyberRTEvt', array(
			'title'			=>	'php - ' . $channel,
			'description'	=>	'Hello ' . $channel . '! from PHP',
		));
		  
		$messages[] = $result;
	}
}
?>

<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<title>PHP</title>
</head>
<body>
	<div class="container mt-5">
		<h1>Ably PHP - Sender</h1>
		<form method="post">
			<input type="hidden" name="action" value="php-submit">
			<input type="submit" class="btn btn-primary" value="Send" />
		</form>
		
		<div class="card p-3 mt-3" style="height: 300px; max-height: 300px; overflow-y: auto;" id="messages-container">
			<?php if( $messages ): ?>
				<?php foreach( $messages as $message ): ?>
					<?php print_r($message); ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>