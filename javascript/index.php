<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<title>Javascript</title>
</head>
<body>
	<div class="container mt-5">
		<h1>Ably Javascript - Receiver</h1>
		<div class="card p-3" style="height: 300px; max-height: 300px; overflow-y: auto;" id="messages-container"></div>
	</div>
	
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<!-- Ably CDN -->	
	<script type="text/javascript" src="//cdn.ably.io/lib/ably.min-1.js"></script>
	
	<script>
	$(document).ready(function(){
		var ably = new Ably.Realtime('Qron-Q.COZFYg:KuZiW6btWOSc0xAd');
		var channels = ['channel_1', 'channel_2', 'channel_3'];
		
		channels.forEach(function(channel_name) {
			var channel = ably.channels.get(channel_name);
			channel.subscribe('CyberRTEvt', function(msg) {
				console.log(msg.data.title);
				console.log(msg.data.description);
				
				$('#messages-container').append('<div>' + JSON.stringify(msg) + '</div>');
			});
		});
	});

	</script>
</body>
</html>